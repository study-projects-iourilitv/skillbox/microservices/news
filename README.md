# News microservice
As part of my educational project for the course "Microservices Architecture" in Skillbox.
Since 26.03.2023

## About
News is a service to manage user's posts has the following features:
- communication with S3 file storing service;
- file descriptions storing and other connected metadata
- requests handling from user service.

## Stack of Technologies
### Core
- JVM: 11
- Gradle: 7.6.1 
- Spring Boot: 2.7.10 [Spring WEB MVC]
- Lombok

### Communication
- HTTP
- REST [Json, Multipart]

### Persist
- Hibernate
- Spring Data [JpaRepository]
- Liquibase
- PostgreSQL

### Documentation
- Diagrams [UML] 
- OpenAPI: 3.0

### Tests
- JUnit: 5
- TestContainers

### Integration with Third Part Services
#### Files Storage
- AWS S3 MinIO SDK
#### Code Quality and Test Covering Checker
- SonarQube
#### Repository Manager
- SonarType Nexus OSS: 3

### DevOps
#### Terminals
- Windows PowerShell
#### Containers
- Docker
#### Pipeline
- Gitlab CI/CD

## Installation
### default profile. With only unit test executing.
Unit test names end with Test.
Use the following command:         
``
Add command
``
### integration-test profile. With only integration tests executing.
Integration test names start with ITest.
Use the following command:      
``
Add command
``

## Configuring
``
Add instructions to configue the app
``

## Usage
``
Add instructions to run the app
``


## Tutorials
All process descriptions of how it was done find in directory .tutorial.                 

## Should be added


## Maintainers
- Iury Litvinenko
