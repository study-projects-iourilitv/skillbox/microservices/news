package com.example.microservices.news;

import lombok.extern.slf4j.Slf4j;
import org.hamcrest.Matchers;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.web.servlet.MockMvc;

import static org.springframework.http.MediaType.APPLICATION_JSON;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@Slf4j
@AutoConfigureMockMvc
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
class NewsApplicationTests {

	@Autowired private MockMvc mockMvc;

	@Test
	void contextLoads() {
		log.info("Testing contextLoads...");
	}

	@Test
	void givenRequestForHealth_thenCorrect_health() throws Exception {
		mockMvc.perform(get("/actuator/health").contentType(APPLICATION_JSON))
				.andDo(print())
				.andExpectAll(
						status().isOk(),
						jsonPath("$.status", Matchers.equalTo("UP"))
				);
	}
}
