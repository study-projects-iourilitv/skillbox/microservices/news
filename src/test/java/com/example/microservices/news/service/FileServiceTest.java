package com.example.microservices.news.service;

import com.amazonaws.AmazonClientException;
import com.example.microservices.news.s3minio.exception.S3PutFileException;
import com.example.microservices.news.s3minio.service.S3StorageService;
import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;
import org.junit.jupiter.api.function.Executable;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.net.URISyntaxException;
import java.util.List;

import static com.example.microservices.news.util.TestUtils.readBytesFromResourceFile;
import static org.apache.commons.lang3.StringUtils.EMPTY;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertIterableEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import static org.springframework.http.MediaType.IMAGE_JPEG_VALUE;

@TestMethodOrder(value = MethodOrderer.MethodName.class)
class FileServiceTest {

    private final S3StorageService storageService = mock(S3StorageService.class);
    private final FileService fileService = new FileServiceImpl(storageService);

    @Test
    void test11_thenOK_getAllFileNames() {
        List<String> expectedList = List.of("image.jpg", "text.txt", "document.doc", "table.xls");
        when(storageService.getAllFileNames()).thenReturn(expectedList);
        var actualList = fileService.getAllFileNames();
        assertIterableEquals(expectedList, actualList);
    }

    @Test
    void test12_thenError_getAllFileNames() {
        Class<AmazonClientException> expectedExceptionClass = AmazonClientException.class;
        when(storageService.getAllFileNames()).thenThrow(expectedExceptionClass);
        Executable actualException = fileService::getAllFileNames;
        assertThrows(expectedExceptionClass, actualException);
    }

    @Test
    void test21_thenOK_uploadFile() throws IOException, URISyntaxException {
        String originalFilename = "img-287KB.jpg";
        String pathname = "files/" + originalFilename;
        byte[] bytes = readBytesFromResourceFile(pathname);
        MultipartFile file = new MockMultipartFile("file", originalFilename, IMAGE_JPEG_VALUE, bytes);
        String expectedETag = "725004a16bfe2bcefecdc30fe133dbcc";
        when(storageService.putFileWithContentAsStream(file)).thenReturn(expectedETag);
        String actualETag = fileService.uploadFile(file);
        assertEquals(expectedETag, actualETag);
    }

    @Test
    void test22_thenError_uploadFile() {
        String errorMessage = String.format("Can't read the file(name: %s)", "null");
        RuntimeException expectedException = new S3PutFileException(errorMessage);
        when(storageService.putFileWithContentAsStream(null)).thenThrow(expectedException);
        String actualErrorMessage = null;
        try {
            fileService.uploadFile(null);
        } catch (S3PutFileException e) {
            actualErrorMessage = e.getMessage();
        }
        assertEquals(expectedException.getMessage(), actualErrorMessage);
    }

    @Test
    void test31_thenOK_deleteFile() {
        String key = "img-287KB.jpg";
        doNothing().when(storageService).delete(key);
        fileService.deleteFile(key);
    }

    @Test
    void test32_thenError_deleteFile() {
        Class<AmazonClientException> expectedExceptionClass = AmazonClientException.class;
        doThrow(expectedExceptionClass).when(storageService).delete(EMPTY);
        Executable actualException = () -> fileService.deleteFile(EMPTY);
        assertThrows(expectedExceptionClass, actualException);
    }
}