package com.example.microservices.news.controller;

import com.example.microservices.news.service.FileService;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.json.JsonMapper;
import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.test.web.servlet.MockMvc;

import java.util.List;

import static com.example.microservices.news.util.TestUtils.readBytesFromResourceFile;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.when;
import static org.springframework.http.HttpMethod.POST;
import static org.springframework.http.MediaType.APPLICATION_JSON;
import static org.springframework.http.MediaType.IMAGE_JPEG_VALUE;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.multipart;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@TestMethodOrder(value = MethodOrderer.MethodName.class)
@WebMvcTest(FileController.class)
class FileControllerTest {
    private static final String BASE_URL = "/api/v1/files";

    private @MockBean FileService fileService;
    private @Autowired MockMvc mockMvc;
    private final ObjectMapper mapper = new JsonMapper();

    @Test
    void test1_thenOk_getAllFilenames() throws Exception {
        List<String> expectedList = List.of("file1", "file2", "file3");
        when(fileService.getAllFileNames()).thenReturn(expectedList);
        String jsonContent = mapper.writeValueAsString(expectedList);
        mockMvc.perform(get(BASE_URL).contentType(APPLICATION_JSON))
                .andDo(print())
                .andExpectAll(
                    status().is2xxSuccessful(),
                    content().json(jsonContent)
                );
    }

    @Test
    void test2_thenOk_uploadFile() throws Exception {
        String originalFilename = "img-287KB.jpg";
        String pathname = "files/" + originalFilename;
        byte[] bytes = readBytesFromResourceFile(pathname);
        MockMultipartFile file = new MockMultipartFile("file", originalFilename, IMAGE_JPEG_VALUE, bytes);
        String expectedETag = "725004a16bfe2bcefecdc30fe133dbcc";
        when(fileService.uploadFile(file)).thenReturn(expectedETag);
        mockMvc.perform(multipart(POST, BASE_URL + "/upload").file(file))
                .andDo(print())
                .andExpectAll(
                        status().is2xxSuccessful(),
                        content().string(expectedETag)
                );
    }

    @Test
    void test3_thenOk_deleteFile() throws Exception {
        String key = "img-287KB.jpg";
        doNothing().when(fileService).deleteFile(key);
        mockMvc.perform(delete(BASE_URL + "/{filename}", key))
                .andDo(print())
                .andExpect(status().is2xxSuccessful());
    }
}