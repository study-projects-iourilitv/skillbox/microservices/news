package com.example.microservices.news.s3minio.service;

import com.amazonaws.AmazonClientException;
import com.amazonaws.services.s3.model.ObjectMetadata;
import com.amazonaws.services.s3.model.PutObjectResult;
import com.amazonaws.services.s3.model.S3Object;
import com.example.microservices.news.s3minio.exception.S3FileNotFoundException;
import com.example.microservices.news.s3minio.exception.S3PutFileException;
import com.example.microservices.news.s3minio.repository.S3Repository;
import com.example.microservices.news.util.TestUtils.TestIOExceptionMultiPartFile;
import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;
import org.junit.jupiter.api.function.Executable;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.web.multipart.MultipartFile;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URISyntaxException;
import java.time.LocalDateTime;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import static com.example.microservices.news.util.AppUtils.dateToLocal;
import static com.example.microservices.news.util.TestUtils.createS3Object;
import static com.example.microservices.news.util.TestUtils.readBytesFromResourceFile;
import static org.apache.commons.lang3.StringUtils.EMPTY;
import static org.junit.jupiter.api.Assertions.assertArrayEquals;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertIterableEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import static org.springframework.http.MediaType.IMAGE_JPEG_VALUE;

@TestMethodOrder(value = MethodOrderer.MethodName.class)
class S3StorageServiceTest {
    private static final String PREFIX = "/";

    private final S3Repository s3Repository = mock(S3Repository.class);
    private final S3StorageService storageService = new S3StorageServiceImpl(s3Repository);

    @Test
    void test11_thenOk_getAllFileNames() {
        List<String> expectedList = List.of("image.jpg", "text.txt", "document.doc", "table.xls");
        when(s3Repository.listKeys(PREFIX)).thenReturn(expectedList);
        var actualList = storageService.getAllFileNames();
        assertIterableEquals(expectedList, actualList);
    }

    @Test
    void test12_thenError_getAllFileNames() {
        Class<AmazonClientException> expectedExceptionClass = AmazonClientException.class;
        when(s3Repository.listKeys(PREFIX)).thenThrow(expectedExceptionClass);
        Executable actualException = storageService::getAllFileNames;
        assertThrows(expectedExceptionClass, actualException);
    }

    @Test
    void test21_thenOk_getFileContentAsStream() throws IOException {
        String key = "img-287KB.jpg";
        String expectedContent = "Some contents";
        when(s3Repository.get(key)).thenReturn(Optional.of(createS3Object(key, expectedContent)));
        byte[] expectedBytes = expectedContent.getBytes();
        byte[] actualBytes = storageService.getFileContentAsStream(key).readAllBytes();
        assertArrayEquals(expectedBytes, actualBytes);
    }

    @Test
    void test22_thenError_getFileContentAsStream() {
        String key = "null";
        S3FileNotFoundException expectedException = new S3FileNotFoundException(key);
        when(s3Repository.get(null)).thenThrow(expectedException);
        String actualErrorMessage = null;
        try {
            storageService.getFileContentAsStream(null);
        } catch (S3FileNotFoundException e) {
            actualErrorMessage = e.getErrorMessage();
        }
        assertEquals(expectedException.getErrorMessage(), actualErrorMessage);
    }

    @Test
    void test31_thenOk_getFileObject() throws UnsupportedEncodingException {
        String key = "img-287KB.jpg";
        String expectedContent = "Some contents";
        Optional<S3Object> expectedS3ObjectOptional = Optional.of(createS3Object(key, expectedContent));
        when(s3Repository.get(key)).thenReturn(expectedS3ObjectOptional);
        Optional<S3Object> actualS3ObjectOptional = storageService.getFileObject(key);
        assertEquals(expectedS3ObjectOptional, actualS3ObjectOptional);
    }

    @Test
    void test32_thenError_getFileObject() {
        String key = "null";
        Class<AmazonClientException> expectedExceptionClass = AmazonClientException.class;
        when(s3Repository.get(key)).thenThrow(expectedExceptionClass);
        Executable actualException = () -> storageService.getFileObject(key);
        assertThrows(expectedExceptionClass, actualException);
    }

    @Test
    void test41_thenOk_putFileWithContentAsStream() throws IOException, URISyntaxException {
        String originalFilename = "img-287KB.jpg";
        String pathname = "files/" + originalFilename;
        byte[] bytes = readBytesFromResourceFile(pathname);
        MultipartFile file = new MockMultipartFile("file", originalFilename, IMAGE_JPEG_VALUE, bytes);
        String expectedETag = "725004a16bfe2bcefecdc30fe133dbcc";
        PutObjectResult putObjectResult = new PutObjectResult();
        putObjectResult.setETag(expectedETag);
        when(s3Repository.put(eq(originalFilename), any(), any())).thenReturn(putObjectResult);
        String actualETag = storageService.putFileWithContentAsStream(file);
        assertEquals(expectedETag, actualETag);
    }

    @Test
    void test42_thenError_putFileWithContentAsStream() {
        String fileName = "null";
        String expectedErrorMessage = String.format("Can't read the file(name: %s)", fileName);
        MultipartFile file = new TestIOExceptionMultiPartFile(fileName, null);
        String actualErrorMessage = null;
        try {
            storageService.putFileWithContentAsStream(file);
        } catch (S3PutFileException e) {
            actualErrorMessage = e.getMessage();
        }
        assertEquals(expectedErrorMessage, actualErrorMessage);
    }

    @Test
    void test43_thenError_putFileWithContentAsStream() {
        Class<AmazonClientException> expectedExceptionClass = AmazonClientException.class;
        when(s3Repository.put(any(), any(), any())).thenThrow(expectedExceptionClass);
        Executable actualException = () -> storageService.putFileWithContentAsStream(
                new MockMultipartFile("null", ByteArrayInputStream.nullInputStream()));
        assertThrows(expectedExceptionClass, actualException);
    }

    @Test
    void test51_thenOk_delete() {
        String key = "img-287KB.jpg";
        doNothing().when(s3Repository).delete(key);
        storageService.delete(key);
    }

    @Test
    void test52_thenError_delete() {
        Class<AmazonClientException> expectedExceptionClass = AmazonClientException.class;
        doThrow(expectedExceptionClass).when(s3Repository).delete(EMPTY);
        Executable actualException = () -> storageService.delete(EMPTY);
        assertThrows(expectedExceptionClass, actualException);
    }

    @Test
    void test61_thenOk_getFileLastModificationTime() throws UnsupportedEncodingException {
        String key = "img-287KB.jpg";
        String content = "Some contents";
        S3Object s3Object = createS3Object(key, content);
        ObjectMetadata metaData = new ObjectMetadata();
        Date date = new Date();
        metaData.setLastModified(date);
        s3Object.setObjectMetadata(metaData);
        when(storageService.getFileObject(key)).thenReturn(Optional.of(s3Object));
        LocalDateTime expectedTime = dateToLocal(date);
        LocalDateTime actualTime = storageService.getFileLastModificationTime(key);
        assertTrue(expectedTime.isEqual(actualTime));
    }

    @Test
    void test62_thenError_getFileLastModificationTime() {
        String key = "null";
        S3FileNotFoundException expectedException = new S3FileNotFoundException(key);
        when(storageService.getFileObject(key)).thenReturn(Optional.empty());
        String actualErrorMessage = null;
        try {
            storageService.getFileLastModificationTime(key);
        } catch (S3FileNotFoundException e) {
            actualErrorMessage = e.getErrorMessage();
        }
        assertEquals(expectedException.getErrorMessage(), actualErrorMessage);
    }
}