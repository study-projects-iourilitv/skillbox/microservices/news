package com.example.microservices.news.s3minio.repository;

import com.amazonaws.SdkClientException;
import com.amazonaws.services.s3.AmazonS3Client;
import com.amazonaws.services.s3.model.AmazonS3Exception;
import com.amazonaws.services.s3.model.ListObjectsV2Result;
import com.amazonaws.services.s3.model.ObjectMetadata;
import com.amazonaws.services.s3.model.PutObjectResult;
import com.amazonaws.services.s3.model.S3Object;
import com.amazonaws.services.s3.model.S3ObjectSummary;
import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;
import org.junit.jupiter.api.function.Executable;

import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.util.List;
import java.util.Optional;

import static com.example.microservices.news.util.TestUtils.createObjectListing;
import static com.example.microservices.news.util.TestUtils.createObjectSummaries;
import static com.example.microservices.news.util.TestUtils.createS3Object;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertIterableEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@TestMethodOrder(value = MethodOrderer.MethodName.class)
class S3RepositoryTest {
    private static final String BUCKET_NAME = "testBucket";
    private static final String PREFIX = "/";

    private final AmazonS3Client s3Client = mock(AmazonS3Client.class);
    private final S3Repository s3Repository = new S3RepositoryImpl(s3Client, BUCKET_NAME);

    @Test
    void test11_thenOk_listKeys() {
        List<String> expectedList = List.of("image.jpg", "text.txt", "document.doc", "table.xls");
        List<S3ObjectSummary> objectSummaries = createObjectSummaries(BUCKET_NAME, expectedList);
        var objectListing = createObjectListing(BUCKET_NAME, PREFIX, objectSummaries);
        when(s3Client.listObjects(BUCKET_NAME, PREFIX)).thenReturn(objectListing);
        var actualList = s3Repository.listKeys(PREFIX);
        assertIterableEquals(expectedList, actualList);
    }

    @Test
    void test12_thenError_listKeys() {
        Class<SdkClientException> expectedExceptionClass = SdkClientException.class;
        when(s3Client.listObjects(BUCKET_NAME, PREFIX)).thenThrow(expectedExceptionClass);
        Executable actualException = () -> s3Repository.listKeys(PREFIX);
        assertThrows(expectedExceptionClass, actualException);
    }

    @Test
    void test21_thenOk_listObjects() {
        List<String> keys = List.of("image.jpg", "text.txt", "document.doc", "table.xls");
        List<S3ObjectSummary> expectedList = createObjectSummaries(BUCKET_NAME, keys);
        ListObjectsV2Result listObjectsV2Result = new ListObjectsV2Result();
        listObjectsV2Result.getObjectSummaries().addAll(expectedList);
        when(s3Client.listObjectsV2(BUCKET_NAME, PREFIX)).thenReturn(listObjectsV2Result);
        var actualList = s3Repository.listObjects(PREFIX);
        assertIterableEquals(expectedList, actualList);
    }

    @Test
    void test22_thenError_listObjects() {
        Class<SdkClientException> expectedExceptionClass = SdkClientException.class;
        when(s3Client.listObjectsV2(BUCKET_NAME, PREFIX)).thenThrow(expectedExceptionClass);
        Executable actualException = () -> s3Repository.listObjects(PREFIX);
        assertThrows(expectedExceptionClass, actualException);
    }

    @Test
    void test31_thenOk_get() throws UnsupportedEncodingException {
        String key = "img-287KB.jpg";
        String content = "Some contents";
        S3Object s3Object = createS3Object(key, content);
        Optional<S3Object> expectedOptional = Optional.of(s3Object);
        when(s3Client.getObject(BUCKET_NAME, key)).thenReturn(s3Object);
        Optional<S3Object> actualOptional = s3Repository.get(key);
        assertTrue(actualOptional.isPresent());
        assertEquals(expectedOptional.get(), actualOptional.get());
    }

    @Test
    void test32_thenError_get() {
        String key = "null";
        Class<SdkClientException> expectedExceptionClass = SdkClientException.class;
        when(s3Client.getObject(BUCKET_NAME, key)).thenThrow(expectedExceptionClass);
        Executable actualException = () -> s3Repository.get(key);
        assertThrows(expectedExceptionClass, actualException);
    }

    @Test
    void test33_thenError_get() {
        String key = "null";
        when(s3Client.getObject(BUCKET_NAME, key)).thenThrow(AmazonS3Exception.class);
        Optional<S3Object> actualOptional = s3Repository.get(key);
        assertNotNull(actualOptional);
    }

    @Test
    void test41_thenOk_put() {
        String key = "img-287KB.jpg";
        InputStream inputStream = InputStream.nullInputStream();
        ObjectMetadata metadata = new ObjectMetadata();
        PutObjectResult expectedResult = new PutObjectResult();
        expectedResult.setMetadata(metadata);
        when(s3Client.putObject(BUCKET_NAME, key, inputStream, metadata)).thenReturn(expectedResult);
        PutObjectResult actualResult = s3Repository.put(key, inputStream, metadata);
        assertEquals(expectedResult, actualResult);
    }

    @Test
    void test42_thenError_put() {
        String key = "null";
        Class<SdkClientException> expectedExceptionClass = SdkClientException.class;
        when(s3Client.putObject(BUCKET_NAME, key, null, null)).thenThrow(expectedExceptionClass);
        Executable actualException = () -> s3Repository.put(key, null, null);
        assertThrows(expectedExceptionClass, actualException);
    }

    @Test
    void test51_thenOk_delete() {
        String key = "img-287KB.jpg";
        doNothing().when(s3Client).deleteObject(BUCKET_NAME, key);
        s3Repository.delete(key);
    }

    @Test
    void test52_thenError_delete() {
        String key = "null";
        Class<SdkClientException> expectedExceptionClass = SdkClientException.class;
        doThrow(expectedExceptionClass).when(s3Client).deleteObject(BUCKET_NAME, key);
        Executable actualException = () -> s3Repository.delete(key);
        assertThrows(expectedExceptionClass, actualException);
    }

    @Test
    void test61_thenOk_bucketName() {
        String actual = s3Repository.bucketName();
        assertEquals(BUCKET_NAME, actual);
    }
}