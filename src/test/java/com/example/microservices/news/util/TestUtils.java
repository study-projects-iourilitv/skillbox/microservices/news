package com.example.microservices.news.util;

import com.amazonaws.services.s3.model.ObjectListing;
import com.amazonaws.services.s3.model.S3Object;
import com.amazonaws.services.s3.model.S3ObjectInputStream;
import com.amazonaws.services.s3.model.S3ObjectSummary;
import com.amazonaws.util.StringInputStream;
import lombok.experimental.UtilityClass;
import org.jetbrains.annotations.NotNull;
import org.springframework.mock.web.MockMultipartFile;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@UtilityClass
public class TestUtils {

    public static byte[] readBytesFromResourceFile(String pathNameOnClasspath) throws IOException, URISyntaxException {
        URL url = Thread.currentThread().getContextClassLoader().getResource(pathNameOnClasspath);
        URI uri = Optional.ofNullable(url).orElseThrow().toURI();
        Path path = Paths.get(uri);
        return Files.readAllBytes(path);
    }

    public static byte[] readBytesFromResourceFile(File file) throws IOException {
        Path path = Paths.get(file.toURI());
        return Files.readAllBytes(path);
    }

    public static S3Object createS3Object(String key, String content) throws UnsupportedEncodingException {
        InputStream inputStream = new StringInputStream(content);
        S3ObjectInputStream expectedS3ObjectInputStream = new S3ObjectInputStream(inputStream, null);
        S3Object s3Object = new S3Object();
        s3Object.setKey(key);
        s3Object.setObjectContent(expectedS3ObjectInputStream);
        return s3Object;
    }

    public static List<S3ObjectSummary> createObjectSummaries(final String bucketName, List<String> keys) {
        return keys.stream().map(key -> {
            S3ObjectSummary objectSummary = new S3ObjectSummary();
            objectSummary.setBucketName(bucketName);
            objectSummary.setKey(key);
            return objectSummary;
        }).collect(Collectors.toList());
    }

    public static ObjectListing createObjectListing(String bucketName, String prefix, List<S3ObjectSummary> objectSummaries) {
        ObjectListing objectListing = new ObjectListing();
        objectListing.setBucketName(bucketName);
        objectListing.setPrefix(prefix);
        objectListing.getObjectSummaries().addAll(objectSummaries);
        return objectListing;
    }

    public static class TestIOExceptionMultiPartFile extends MockMultipartFile {
        public TestIOExceptionMultiPartFile(String name, byte[] content) {
            super(name, content);
        }

        @Override
        public @NotNull InputStream getInputStream() throws IOException {
            throw new IOException();
        }
    }
}
