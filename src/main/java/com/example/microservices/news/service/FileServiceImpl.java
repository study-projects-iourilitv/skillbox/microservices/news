package com.example.microservices.news.service;

import com.example.microservices.news.s3minio.service.S3StorageService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

@Slf4j
@RequiredArgsConstructor
@Service
public class FileServiceImpl implements FileService {

    private final S3StorageService storageService;

    @Override
    public List<String> getAllFileNames() {
        return (List<String>) storageService.getAllFileNames();
    }

    @Override
    public String uploadFile(MultipartFile file) {
        return storageService.putFileWithContentAsStream(file);
    }

    @Override
    public void deleteFile(String key) {
        storageService.delete(key);
    }
}
