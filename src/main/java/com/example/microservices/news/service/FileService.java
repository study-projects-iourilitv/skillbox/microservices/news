package com.example.microservices.news.service;

import org.springframework.web.multipart.MultipartFile;

import java.util.List;

public interface FileService {
    List<String> getAllFileNames();
    String uploadFile(MultipartFile file);
    void deleteFile(String key);
}
