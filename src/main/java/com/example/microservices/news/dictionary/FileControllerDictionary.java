package com.example.microservices.news.dictionary;

import lombok.experimental.UtilityClass;

@UtilityClass
public class FileControllerDictionary {
    public static final String EXAMPLE_RESPONSE_GET_ALL_FILENAMES_OK_200 = "[\"image.jpg\",\"text.txt\",\"document.doc\",\"table.xls\"]";
}
