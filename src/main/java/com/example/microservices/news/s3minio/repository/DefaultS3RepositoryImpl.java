package com.example.microservices.news.s3minio.repository;

import com.amazonaws.services.s3.AmazonS3;
import com.example.microservices.news.s3minio.property.S3Properties;
import org.springframework.stereotype.Component;

@Component
public class DefaultS3RepositoryImpl extends S3RepositoryImpl {
    public DefaultS3RepositoryImpl(AmazonS3 s3Client, S3Properties properties) {
        super(s3Client, properties.getDemoBucketName());
    }
}
