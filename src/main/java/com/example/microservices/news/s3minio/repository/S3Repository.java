package com.example.microservices.news.s3minio.repository;

import com.amazonaws.services.s3.model.ObjectMetadata;
import com.amazonaws.services.s3.model.PutObjectResult;
import com.amazonaws.services.s3.model.S3Object;
import com.amazonaws.services.s3.model.S3ObjectSummary;

import java.io.InputStream;
import java.util.Collection;
import java.util.Optional;

public interface S3Repository {
    Collection<String> listKeys(String prefix);
    Collection<S3ObjectSummary> listObjects(String prefix);
    Optional<S3Object> get(String key);
    PutObjectResult put(String key, InputStream inputStream, ObjectMetadata metadata);
    void delete(String key);
    String bucketName();
}
