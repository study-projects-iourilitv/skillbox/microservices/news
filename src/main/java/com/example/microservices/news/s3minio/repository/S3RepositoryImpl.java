package com.example.microservices.news.s3minio.repository;

import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.model.AmazonS3Exception;
import com.amazonaws.services.s3.model.ObjectMetadata;
import com.amazonaws.services.s3.model.PutObjectResult;
import com.amazonaws.services.s3.model.S3Object;
import com.amazonaws.services.s3.model.S3ObjectSummary;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;

import java.io.InputStream;
import java.util.Collection;
import java.util.Optional;
import java.util.stream.Collectors;

@Slf4j
@AllArgsConstructor
public class S3RepositoryImpl implements S3Repository {
    private final AmazonS3 s3Client;
    private final String bucketName;

    @Override
    public Collection<String> listKeys(String prefix) {
        return s3Client.listObjects(bucketName, prefix)
                .getObjectSummaries().stream()
                .map(S3ObjectSummary::getKey)
                .collect(Collectors.toList());
    }

    @Override
    public Collection<S3ObjectSummary> listObjects(String prefix) {
        return s3Client.listObjectsV2(bucketName, prefix).getObjectSummaries();
    }

    @Override
    public Optional<S3Object> get(String key) {
        try {
            return Optional.of(s3Client.getObject(bucketName, key));
        } catch (AmazonS3Exception e) {
            log.error("Service S3 Could Not Complete get(bucketName:{}, key:{}) Operation", bucketName, key);
            return Optional.empty();
        }
    }

    @Override
    public PutObjectResult put(String key, InputStream inputStream, ObjectMetadata metadata) {
        return s3Client.putObject(bucketName, key, inputStream, metadata);
    }

    @Override
    public void delete(String key) {
        s3Client.deleteObject(bucketName, key);
    }

    @Override
    public String bucketName() {
        return this.bucketName;
    }
}
