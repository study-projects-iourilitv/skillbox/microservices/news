package com.example.microservices.news.s3minio.service;

import com.amazonaws.services.s3.model.S3Object;
import org.springframework.web.multipart.MultipartFile;

import java.io.InputStream;
import java.time.LocalDateTime;
import java.util.Collection;
import java.util.Optional;

public interface S3StorageService {
    Collection<String> getAllFileNames();
    InputStream getFileContentAsStream(String key);
    Optional<S3Object> getFileObject(String key);
    String putFileWithContentAsStream(MultipartFile file);
    void delete(String key);
    LocalDateTime getFileLastModificationTime(String key);
}
