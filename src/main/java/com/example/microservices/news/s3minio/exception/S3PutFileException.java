package com.example.microservices.news.s3minio.exception;

public class S3PutFileException extends RuntimeException {
    public S3PutFileException(String message) {
        super(message);
    }
}
