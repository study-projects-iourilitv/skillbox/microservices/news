package com.example.microservices.news.s3minio.exception;

import com.amazonaws.services.s3.model.AmazonS3Exception;

public class S3FileNotFoundException extends AmazonS3Exception {
    private static final String messageTemplate = "File Object(key: %s) Not Found";

    public S3FileNotFoundException(String key) {
        super(String.format(messageTemplate, key));
    }

    public S3FileNotFoundException(String message, Exception cause) {
        super(message, cause);
    }
}
