package com.example.microservices.news.s3minio.property;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

@Data
@ConfigurationProperties(prefix = "aws.s3.minio")
@Component
public class S3Properties {
    private String endpoint;
    private String signer;
    private String region;
    private String accessKey;
    private String secretKey;
    private String demoBucketName;
}
