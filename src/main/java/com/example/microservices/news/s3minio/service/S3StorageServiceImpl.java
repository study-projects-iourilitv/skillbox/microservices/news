package com.example.microservices.news.s3minio.service;

import com.amazonaws.services.s3.model.ObjectMetadata;
import com.amazonaws.services.s3.model.S3Object;
import com.example.microservices.news.s3minio.exception.S3FileNotFoundException;
import com.example.microservices.news.s3minio.exception.S3PutFileException;
import com.example.microservices.news.s3minio.repository.S3Repository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.lang.NonNull;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.io.InputStream;
import java.time.LocalDateTime;
import java.util.Collection;
import java.util.Date;
import java.util.Optional;

import static com.example.microservices.news.util.AppUtils.dateToLocal;

@Slf4j
@RequiredArgsConstructor
@Service
public class S3StorageServiceImpl implements S3StorageService {

    private final S3Repository s3Repository;

    @Override
    public Collection<String> getAllFileNames() {
        return s3Repository.listKeys("/");
    }

    @Override
    public InputStream getFileContentAsStream(String key) {
        return s3Repository.get(key)
                .map(S3Object::getObjectContent)
                .orElseThrow(() -> new S3FileNotFoundException(key));
    }

    @Override
    public Optional<S3Object> getFileObject(String key) {
        return s3Repository.get(key);
    }

    @Override
    public String putFileWithContentAsStream(@NonNull MultipartFile file) {
        InputStream inputStream;
        try {
            inputStream = file.getInputStream();
        } catch (IOException e) {
            String errorMessage = String.format("Can't read the file(name: %s)", file.getName());
            log.error(errorMessage);
            throw new S3PutFileException(errorMessage);
        }
        String key = file.getOriginalFilename();
        long size = file.getSize();
        String contentType = file.getContentType();
        log.info("putFileWithContentAsStream(). Putting the file... key: {}, contentLength: {}, contentType: {}", key, size, contentType);
        ObjectMetadata metadata = new ObjectMetadata();
        metadata.setContentLength(size);
        metadata.setContentType(contentType);
        var result = s3Repository.put(key, inputStream, metadata);
        return result.getETag();
    }

    @Override
    public void delete(String key) {
        log.info("Deleting the file... key:{}", key);
        s3Repository.delete(key);
    }

    @Override
    public LocalDateTime getFileLastModificationTime(String key) {
        Date lastModified = getFileObject(key)
                .orElseThrow(() -> new S3FileNotFoundException(key))
                .getObjectMetadata()
                .getLastModified();
        return dateToLocal(lastModified);
    }
}
