package com.example.microservices.news.controller;

import com.example.microservices.news.service.FileService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.ExampleObject;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

import static com.example.microservices.news.dictionary.FileControllerDictionary.EXAMPLE_RESPONSE_GET_ALL_FILENAMES_OK_200;
import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;
import static org.springframework.http.MediaType.APPLICATION_OCTET_STREAM_VALUE;
import static org.springframework.http.MediaType.MULTIPART_FORM_DATA_VALUE;
import static org.springframework.http.MediaType.TEXT_PLAIN_VALUE;

@Tag(name = "News", description = "CRUD operations with files")
@RequiredArgsConstructor
@RestController
@RequestMapping(value = "/api/v1/files")
public class FileController {

    private final FileService fileService;

    @Operation(summary = "Getting a list of file names")
    @ApiResponse(responseCode = "200", description = "Returns all file names",
        content = @Content(mediaType = APPLICATION_JSON_VALUE, schema = @Schema(allOf = String.class),
            examples = @ExampleObject(value = EXAMPLE_RESPONSE_GET_ALL_FILENAMES_OK_200)))
    @GetMapping
    public ResponseEntity<List<String>> getAllFilenames() {
        List<String> fileNames = fileService.getAllFileNames();
        return ResponseEntity.ok(fileNames);
    }

    @Operation(summary = "Uploading a file")
    @ApiResponse(responseCode = "200", description = "Uploads a file to the storage and returns its eTag",
        content = @Content(mediaType = TEXT_PLAIN_VALUE, schema = @Schema(implementation = String.class),
            examples = @ExampleObject(value = "725004a16bfe2bcefecdc30fe133dbcc")))
    @PostMapping(value = "/upload", consumes = MULTIPART_FORM_DATA_VALUE, produces = TEXT_PLAIN_VALUE)
    public ResponseEntity<String> uploadFile(@Parameter(description = "Name of a file to upload", required = true,
            content = @Content(mediaType = APPLICATION_OCTET_STREAM_VALUE)) @RequestParam("file") MultipartFile file) {
        String eTag = fileService.uploadFile(file);
        return ResponseEntity.ok(eTag);
    }

    @Operation(summary = "Deleting a file")
    @ApiResponse(responseCode = "200", description = "Deletes a file in the storage",
        content = @Content(mediaType = APPLICATION_JSON_VALUE))
    @DeleteMapping("/{filename}")
    public ResponseEntity<Void> deleteFile(@Parameter(description = "Name of a file to delete in the storage",
            required = true) @PathVariable("filename") String key) {
        fileService.deleteFile(key);
        return ResponseEntity.ok().build();
    }
}
