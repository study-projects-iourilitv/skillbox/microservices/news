# AWS S3 MinIO: V4. Deploying in Docker locally
Files Storage.

## Prepare Environment
Follow the requirements in .tutorial/gitlab-prepare-environment.md.      
Do additionally.
## Run 2-nd PS in the project directory
``
cd D:\projects\skillbox\microservices\news
``          
Next working in D:\projects\skillbox\microservices\news>

## Deploy by docker container
### Pull docker image
Result:     
``
PS C:\WINDOWS\system32> docker pull bitnami/minio:latest
latest: Pulling from bitnami/minio
41c13aa60063: Pull complete
Digest: sha256:4d224d06784e34089d4e46964a96ab6cc07ca6d8c29eaaba263b58f33e3f1a02
Status: Downloaded newer image for bitnami/minio:latest
docker.io/bitnami/minio:latest
``

### Run container
Here:      
-d - the container starts as a daemon so the PS will be unlocked after the process done     
--name minio - a name of container with the service to show in Docker       
--publish 9090:9000 - {container's port}:{service's port} for access to the service's API       
--publish 9091:9001 - {container's port}:{service's port} for admin GUI     
--env MINIO_ROOT_USER="minio_admin" - environment variable of username with default value for admin     
--env MINIO_ROOT_PASSWORD="minio_admin" - environment variable of password with default value for admin     
--volume miniodata:/data - {volume name}/{directory in container}a volume out of the service's docker container          
bitnami/minio:latest - docker image of the service      
``
docker run -d --name minio --publish 9090:9000 --publish 9091:9001 --env MINIO_ROOT_USER="minio_admin" --env MINIO_ROOT_PASSWORD="minio_admin" --volume miniodata:/data bitnami/minio:latest
``          
Result:     
``
PS C:\WINDOWS\system32> docker run -d --name minio --publish 9090:9000 --publish 9091:9001 --env MINIO_ROOT_USER="minio_admin" --env MINIO_ROOT_PASSWORD="minio_admin" --volume miniodata:/data bitnami/minio:latest
e82e97903ea1b7de3b37e621b042a7dcc7869f2243d100d016c9840c7a96a157
``

## Work in MinIO
### Check in a browser
- Go to http://localhost:9091     
- Sign In with: minio_admin / minio_admin
### Create new bucket: mybucket ("Create Bucket" button in Menu / Buckets)
### Create access key
- in Menu / Access Keys click "Create access key" button
- click "Create" button in the next "Create Access Key" step
- click "Download for import" button in pop up "New Access Key Created" 
- choose a directory to save credentials.json with the keys