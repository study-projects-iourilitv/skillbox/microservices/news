# Gradle Running Tutorial

## Project
### Settings
Project / Settings / Build, Execution, Deployment / Build Tools / Gradle:           
Gradle user home: {user}/.gradle            
Gradle JVM: JAVA_HOME=11            
### Generated files location
./build

## Gradle Version: 7.6.1
## Commands (in PowerShell in project directory)
### Build
``
./gradlew build
``

#### In IDEA create new run configurations with environment variables
##### To build with gradle
See .run/build.run.xml
#### To run the application (needed build.run.xml as well)
See .run/NewsApp.run.xml        

### Logging
``
> Run with --stacktrace option to get the stack trace.
> Run with --info or --debug option to get more log output.
> Run with --scan to get full insights.
``
