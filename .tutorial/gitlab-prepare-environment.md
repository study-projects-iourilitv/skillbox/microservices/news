# Prepare environment for CI/CD on gitlab. 
Do operations if they still have not done

## 1. Run Docker
### Install Docker Desktop for Window
Source: https://docs.docker.com/desktop/install/windows-install/
### Run docker-demon
Run Docker Desktop for Window for it

## 2. Run gitlab runner
### Run PowerShell (PS) as administrator
### In PS go to D:\Software\gitlab\GitLab-Runner
``
cd D:\Software\gitlab\GitLab-Runner
``      
Next working in PS D:\Software\gitlab\GitLab-Runner>            
### Start default gitlab runner with PS: gitlab-win64-local
``
./gitlab-runner.exe start
``      
Result:     
``
PS D:\Software\gitlab\GitLab-Runner> .\gitlab-runner.exe start
Runtime platform                                    arch=amd64 os=windows pid=3280 revision=12335144 version=15.8.0
``

## 3. Run in Docker DeskTop the following containers:
### sonarqube - CodeQuality checking server
### nexus - Sonartype Nexus Repository Manager OSS
### minio - 

*************************************************************           
## Settings
### Gitlab
#### Paths
##### Group study-projects-iourilitv/skillbox/microservices (Created 25.03.23)
##### Project study-projects-iourilitv/skillbox/microservices/news (Created 26.03.23)

#### Variables
##### Group study-projects-iourilitv/skillbox/microservices
- NEXUS_REPO_URL: http://localhost:8081/repository/skillbox-mvn-hosted-
- NEXUS_REPO_USER: admin
- NEXUS_REPO_PASS: nexus
- SONAR_HOST_URL: http://localhost:9000
- SONAR_LOGIN_TOKEN: squ_1af8d87893a5ce56f3b5975216a103a804e97b27
- S3_MINIO_ENDPOINT: http://localhost:9090
- S3_MINIO_ACCESS_KEY: 0VqUQCuMEcRl19Wj
- S3_MINIO_SECRET_KEY: Cm4qHAWZjmtTaKiTOLcOE4P5xmDoss59

### Default gitlab runners with PS:
#### gitlab-study-projects-iourilitv-win64-local for group study-projects-iourilitv/skillbox/microservices
