# Created a new ROLE micronews: 
CREATE ROLE micronews WITH
	LOGIN
	NOSUPERUSER
	CREATEDB
	NOCREATEROLE
	INHERIT
	NOREPLICATION
	CONNECTION LIMIT -1
	PASSWORD 'xxxxxx';
GRANT pg_read_all_settings TO micronews WITH ADMIN OPTION;
COMMENT ON ROLE micronews IS 'spring:
datasource:
url: jdbc:postgresql://localhost:5432/news?currentSchema=news_scheme
username: micronews
password: micronews';

# Created a new Database:
CREATE DATABASE news
    WITH 
    OWNER = micronews
    ENCODING = 'UTF8'
    CONNECTION LIMIT = -1;

COMMENT ON DATABASE news
    IS 'Database for news microservices';

# Created new Scheme:
CREATE SCHEMA news_scheme
    AUTHORIZATION micronews;

COMMENT ON SCHEMA news_scheme
    IS 'currentSchema for news microservices';