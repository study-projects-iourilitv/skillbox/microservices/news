# Configure Liquibase to start

## Add into application.yml some properties
### Add datasource properties
### Use default property (No need to set explicitly)
``
spring:
  liquibase:
    enabled:true
``

## Configure liquibase
### Create resources/db/changelog/db.changelog-master.yaml
### Set the path and disable errors producing condition property
``
databaseChangeLog:
  - includeAll:
      path: classpath:db/changelog/changesets/init
      errorIfMissingOrEmpty: false
``
